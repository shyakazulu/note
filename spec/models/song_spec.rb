require 'rails_helper'
require 'spec_helper'
RSpec.describe Song, type: :model do
  let(:valid_attributes) {
  	{
  		title: "vcr",
  		rating: 3,
  		artist: "tyler the creator"
  	}
  }

  context "relationships" do
    it {should belong_to(:album)}
    it {should belong_to(:user)}
    it {should belong_to(:playlist)}
    it {should have_many(:categories)}
  end
  context "validations" do
    let(:song) {Song.new(valid_attributes)}

    before do
      User.create(valid_attributes)
    end
    it "requires a title" do
      expect(user).to validate_presence_of(:title)
    end

    it "requires an artist name" do
      expect(user).to valiidate_presence_of(:artist)
    end 	
  end
end
