require 'rails_helper'
require 'spec_helper'
RSpec.describe Artist, type: :model do
  context "relationshipps" do 
    it {should have_many(:songs)}
    it {should have_many(:albums)}
  end
end
