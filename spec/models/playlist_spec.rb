require 'rails_helper'
require 'spec_helper'
RSpec.describe Playlist, type: :model do
context "relationships" do	
   it {should belong_to(:user)}
   it {should have_many(:songs)}
end
end
