require 'rails_helper'
require 'spec_helper'
RSpec.describe Album, type: :model do
  context "relationships" do
    it {should have_many(:songs)}
    it {should belong_to(:user)}
    it {should have_and_belong_to_many(:categories)}
  end
end
