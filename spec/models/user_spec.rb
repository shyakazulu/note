require 'rails_helper'
require 'spec_helper'
RSpec.describe User, type: :model do
	let(:valid_attributes) {
		{
		first_name: "John",
		last_name: "Doe",
		email: "john@thetestapp.com",
		password: "iamcool",
		password_confirmation: "iamcool",
    gender: "M",
    location: "usa"
		}
	}

  context "relationships" do
  	it {should have_many(:albums)}
  	it {should have_many(:songs)}
  	it {should have_many(:playlists)}
  end

  context "Validations" do
    let(:user) { User.new(valid_attributes) }

    before do
      User.create(valid_attributes)
    end
    
    it "requires an email" do 
       expect(user).to validate_presence_of(:email)
    end
    
    it "requires a unique email" do
       expect(user).to validate_uniqueness_of(:email)
    end

    it "requires a unique email (case sensitive)" do
       user.email = "JOHN@THETESTAPP.COM"
       expect(user).to validate_uniqueness_of(:email)
    end

    it  "requires the email to look like an email" do
       user.email = "john"
       expect(user).to_not be_valid
    end
    
    it "requires a name" do
  	  expect(user).to validate_presence_of(:first_name)
  	  expect(user).to validate_presence_of(:last_name)
  	end
  end


  # describe "downcase_email" do
  #   it "makes the email attribute lower case" do
  #   	user = User.new(valid_attributes.merge(email: "JOHN@THETESTAPP.COM"))
  #   	expect{ user.downcase_email }.to change{ user.email }.
  #   	from("JOHN@THETESTAPP.COM").
  #   	to("john@thetestapp.com")
  #   end

  #   it "downcases an email before saving" do
  #    user = User.new(valid_attributes)
  #    user.email = "MIKE@THETESTAPP.COM"
  #    expect(user.save).to be_true
  #    expect(user.email).to eq("mike@thetestapp.com")
  #   end
  # end
  
end

