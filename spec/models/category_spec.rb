require 'rails_helper'
require 'spec_helper'
RSpec.describe Category, type: :model do
  context"relationships" do
    it {should have_many(:playlist)}
    it {should have_many(:album)}
    it {should have_many(:sons)}
  end
end
