FactoryGirl.define do
	factory :user do
		sequence(:email) { |n| "foo#{n}@example.com"}
		password "secret"
		password_confirmation "secret"
		first_name "John"
		last_name  "Doe"
    end
end