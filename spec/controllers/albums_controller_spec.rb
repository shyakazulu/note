require 'rails_helper'
require 'spec_helper'

RSpec.describe AlbumsController, type: :controller do
  #this should return the minimal set of values required to 
  #create a valid albums
  let(:valid_attributes){{
  	"title"  => "Mystring",
  	"tags"   => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro maxime impedit id quibusdam. Sapiente tenetur",
  	"rating" => 5,
  	"category" => "My_category"
  	}}

  	let(:valid_session) { {} }
  	let!(:user) { create(:user)}

  	# before do
   #    sign_in(user)
  	# end

  	# describe "GET index" do
   #    context "logged in" do
   #    	it "assigns all albums as @albums" do
   #        album = user.albums.create! valid_attributes
   #        get :index, {}, valid_session
   #        assigns(:albums).should eq([albums])
   #        expect(assigns(:albums).map(&:user)).to eq([user])

   #      end
   #    end
  	# end
end
