require 'rails_helper'
require 'spec_helper'

describe UsersController, type: :controller do
  let(:valid_attributes) { { 
    "first_name" => "MyString",
    "last_name" => "LastName",
    "email" => "email@example.com",
    "password" => "password12345",
    "password_confirmation" => "password12345",
    "gender" => "M",
    "age" => 23,
    "location" => "usa"
  } }

  let(:valid_session) { {} }

  describe "GET new" do
    it "assigns a new user as @user" do
      get :new, {}, valid_session
      assigns(:user).should be_a_new(User)
    end
  end
  describe "GET edit" do
    it "assigns the requested user as @user" do
      user = User.create! valid_attributes
      get :edit, {:id => user.to_param}, valid_session 
      assigns(:user).should eq(user)
    end
  end
end
