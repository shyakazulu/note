# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150425021101) do

  create_table "albums", force: true do |t|
    t.string   "title"
    t.integer  "artist_id"
    t.integer  "rating"
    t.integer  "user_id"
    t.text     "tags",       default: " "
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "albums", ["title"], name: "index_albums_on_title"

  create_table "artists", force: true do |t|
    t.string   "name"
    t.text     "tags"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories_songs", force: true do |t|
    t.integer "category_id", null: false
    t.integer "song_id",     null: false
  end

  create_table "playlists", force: true do |t|
    t.string   "title"
    t.integer  "user_id"
    t.text     "description"
    t.text     "tags",        default: " "
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "playlists", ["title"], name: "index_playlists_on_title"

  create_table "songs", force: true do |t|
    t.string   "title"
    t.integer  "rating"
    t.integer  "album_id"
    t.integer  "artist_id"
    t.integer  "user_id"
    t.text     "tags",        default: " "
    t.integer  "playlist_id"
    t.integer  "duration"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "songs", ["title"], name: "index_songs_on_title"

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "location"
    t.integer  "age"
    t.string   "gender"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email"

end
