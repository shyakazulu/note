class CategoriesSongsJoin < ActiveRecord::Migration
  def up
  	create_table :categories_songs do |t|
      t.references :category, null: false
      t.references :song,     null: false
  	end                   
  end
  def down
    drop_table :categories_songs
  end
end
