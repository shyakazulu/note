class CreateAlbums < ActiveRecord::Migration
  def change
    create_table   :albums do |t|
      t.string     :title
      t.integer    :artist_id
      t.integer    :rating
      t.references :user
      t.text       :tags, default: " "
      t.string     :category
      t.timestamps
    end
    add_index :albums, :title

  end
end
