class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.timestamps
    end
    Category.create(name: 'Rock')
    Category.create(name: 'Rnb')
    Category.create(name: 'HipHop/Rap')
    Category.create(name: 'Classic')
    Category.create(name: 'Jazz')
    Category.create(name: 'Pop')
    Category.create(name: 'Religious')
    Category.create(name: 'Alternative/Rock')
    Category.create(name: 'PunkRock')
    Category.create(name: 'Blues')
    Category.create(name: 'Country')
    Category.create(name: 'Latin')
  end
end
