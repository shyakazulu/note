class CreateSongs < ActiveRecord::Migration
  def change
    create_table   :songs do |t|
      t.string     :title
      t.integer    :rating
      t.references :album
      t.integer    :artist_id
      t.references :user
      t.text       :tags, default: " "
      t.integer    :playlist_id
      t.integer    :duration
      t.timestamps
    end
    add_index :songs, :title
  end
end
