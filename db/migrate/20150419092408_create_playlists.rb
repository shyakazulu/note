class CreatePlaylists < ActiveRecord::Migration
  def change
    create_table   :playlists do |t|
      t.string     :title
      t.references :user
      t.text       :description
      t.text       :tags, default: " "
      t.timestamps
    end
      add_index    :playlists, :title
  end
end
