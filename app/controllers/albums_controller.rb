class AlbumsController < ApplicationController
   
   def index
   	
   end
   def show
   	
   end
   def new
   	
   end
   def create
   	
   end
   def edit
   	
   end
   def update
   	
   end
   def delete
   	
   end
   def destroy
   	
   end

   private
  
   def album_params
     params.require(:album).permit(:title, :position, :artist_id, :rating, :tags, :user_id, :category )
   end
end
