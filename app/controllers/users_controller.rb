class UsersController < ApplicationController
   before_action :set_user, only: [:show, :edit, :update, :destroy]
   def index
   	
   end
   def show
   	
   end
   def new
   	@user = User.new(user_params)
   end
   def create
   	@user = User.new(user_params)
   end
   def edit
   	
   end
   def update
   	
   end
   def delete
   	
   end
   def destroy
   	
   end

   private
   def set_user
   	 @user = User.find(params[:id])
   end
   def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :gender, :location)
   end
end
