class User < ActiveRecord::Base
	has_many :albums
	has_many :songs
	has_many :playlists

	has_secure_password
	before_save :downcase_gender
	before_save :downcase_email
    
    EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\z/i
	validates :email, presence: true,
	                  uniqueness: true,
	                  length: {within: 4..150},
	                  format: EMAIL_REGEX

	validates :first_name, presence: true,
	                       length: {maximum: 100}
	validates :last_name,  presence: true,
	                       length: {maximum: 100}
	validates :gender,     presence: true
	                       #format: {[MFmf]}
	validates :location, length: {maximum: 1000}

	def downcase_gender
	  self.gender = gender.downcase
	end
	
	def downcase_email
     self.email = self.email.downcase!
	end
	
end