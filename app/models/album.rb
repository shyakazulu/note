class Album < ActiveRecord::Base
	belongs_to :artist
	belongs_to :user
	has_many   :songs
	has_and_belongs_to_many :categories
    
    before_save :add_default_tags
	validate :title, presence: true,
                     length: {maximum: 100}
    validates :tags, length: {maximum: 4000}
    #adding default tags
    def add_default_tags
    	@title = self.title.to_s
    	@songs = self.songs.all
    	@artist = Artist.find(self.artist_id).name.to_s
        self.tags = (tags + @title + @artist).downcase
    end
end
