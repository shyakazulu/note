class Song < ActiveRecord::Base
	belongs_to :album
	belongs_to :user
	belongs_to :artist
	belongs_to :playlist
	has_and_belongs_to_many :categories

    before_save :add_default_tags
    validates :title, presence: true,
                      length: {maximum: 100}

    validate :tags,   length: {maximum: 4000}
    
    def add_default_tags
       #@category = self.category.to_s
       @title = self.tile.to_s
       @artist = Artist.find(self.artist_id).name.to_s
       self.tags = (tags + @title + @artist).downcase
    end	
end
