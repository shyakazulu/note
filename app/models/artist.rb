class Artist < ActiveRecord::Base
	has_many :songs
	has_many :albums

	before_save :downcase_name
	before_save :create_tags
    validates :name, presence: true,
                     length: {maximum: 500}
	def downcase_name
	   self.name = name.downcase
	end

	def create_tags
      @songs = self.songs.all
      @tags = @songs.map {|song| song.tags}
      @tags.flatten!.uniq!
      @tags.each do |t|
        self.tags += t.to_s	
      end
    end

end
