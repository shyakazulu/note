class Playlist < ActiveRecord::Base
  has_many :songs
  belongs_to :user

  before_save :create_tags

  validates :title, presence: true
  validates :description, length: {maximum: 400}

  def create_tags
    @songs = self.songs.all
    @tags = @songs.map {|song| song.tags}
    @tags.flatten!.uniq!
    @tags.each do |t|
       self.tags += t.to_s	
    end
  end
end
