class Category < ActiveRecord::Base
	has_and_belongs_to_many :songs
	has_and_belongs_to_many :albums
	has_and_belongs_to_many :playlists

	validates :name, presence:   true,
	                 uniqueness: true,
	                 length: {maximum: 100}   

end